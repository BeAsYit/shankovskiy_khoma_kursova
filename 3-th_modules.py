from marvel.marvel import Marvel
from marvel.character import MarvelObject
import array




class Character(MarvelObject):
    """
    Character object
    Takes a dict of character attrs
    """
    _resource_url = 'characters'

    @property
    def id(self):
        return self.dict['id']

    @property
    def name(self):
        return self.dict['name']

m = Marvel('287be8ad61e7ec8dd709c839b9f70868', '6eec326658211ec38e2cd085a2243d87fd29cd4b')

I_M = Character(m, "Iron man")
C_A = Character(m, "Captain America")


list_ = array.array(I_M.id, C_A.id)


result = []
response = m.get_comics(characters="{},{}".format(list_[0], list_[1]), limit="1")
print response.data.total
events = response.data.results
for i in range(0, len(events)):
    if events[i].series['name'] not in result:
        result.append(events[i].series['name'])
        print events[i].series['name']

print m.get_character()