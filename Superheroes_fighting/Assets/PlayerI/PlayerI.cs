﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerI : MonoBehaviour
{
    public MenuManeger or;

    // Movement
    public float movementSpeed = 5f;
    public float jumpHeight = 10f;
	[SerializeField]
	private GameObject gameOverUI;

    // Health 
    [SerializeField]
    private Stat health;
    
    // Current health value - private to not allow to change it directly via instance
    
    [HideInInspector] public bool dead = false;

    // Physics
    // Field to store reference to Rigidbody2D component attached to player ojbect
    private Rigidbody2D playerRigidbody;

    // Field to store reference to Animator component attached to player object
    private Animator playerAnim;

    private void Awake()
    {
        health.Initialize();
    }
    // Use this for initialization
    void Start()
    {
        // Set health to max value on start



        dead = false;

        


        // Asign animator reference
        playerAnim = GetComponent<Animator>();

        // Asign rigidbody reference
        playerRigidbody = GetComponent<Rigidbody2D>();

    }

    public void TakeDamage(float amount)
    {
        if (!dead)
        {
            health.CurrentVal -= amount;
            // Check to make sure that UI won't get negative value
            if (health.CurrentVal <= 0)
            {
                health.CurrentVal = 0;
                Die();
            }

        }
    }

    public void Die()
    {
        dead = true;
        StartCoroutine(DeathAnimation());
    }

	public void EndGame (){
		gameOverUI.SetActive (true);

	}

    private IEnumerator DeathAnimation()
    {
        // Turn off rigidbody(gravity and stuff)
        playerRigidbody.isKinematic = true;

        // Trigger death animation
        playerAnim.SetTrigger("death");
		EndGame ();

        yield return new WaitForSeconds(2);


    }
}
