﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Platforms : MonoBehaviour {

    private BoxCollider2D playerColliderI;
    private BoxCollider2D playerColliderC;
    
    [SerializeField]
    private BoxCollider2D platformCollider;
    [SerializeField]
    private BoxCollider2D platformTrigger;

    // Use this for initialization
    void Start () {

        playerColliderI = GameObject.Find("PlayerI").GetComponent<BoxCollider2D>();
        playerColliderC = GameObject.Find("PlayerC").GetComponent<BoxCollider2D>();

        Physics2D.IgnoreCollision(platformCollider, platformTrigger, false);
	}
	
	void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.name == "PlayerI")
        {
            Physics2D.IgnoreCollision(platformCollider, playerColliderI, true);
        }

        else if (other.gameObject.name == "PlayerC")
        {
            Physics2D.IgnoreCollision(platformCollider, playerColliderC, true);
        }

    }
    void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.name == "PlayerI")
        {
            Physics2D.IgnoreCollision(platformCollider, playerColliderI, false);
        }

        else if (other.gameObject.name == "PlayerC")
        {
            Physics2D.IgnoreCollision(platformCollider, playerColliderC, false);
        }
    }

} 

