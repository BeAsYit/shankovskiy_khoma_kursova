﻿using UnityEngine;
using System.Collections;

public class ProjectileC : MonoBehaviour
{

    public float projectileSpeed = 20f;

    public float destroyTime = 3f;
    public float projectileDamage = 10;
    public Transform childProjectile;

    public LayerMask hitLayer;

   
    // Use this for initialization
    void Start()
    {
        Destroy(this.gameObject, destroyTime);
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(transform.right * projectileSpeed * Time.deltaTime * transform.right.x);
        childProjectile.Rotate(new Vector3(0, 0, 30) * Time.deltaTime * projectileSpeed);

        RaycastHit2D hit = Physics2D.Raycast(transform.position, transform.right * transform.right.x, projectileSpeed * Time.deltaTime, hitLayer);
        if (hit)
        {
            PlayerI hitEnemy = hit.collider.GetComponent<PlayerI>();
            if (hitEnemy != null)
            {
                hitEnemy.TakeDamage(projectileDamage);
            }
			else if(hit.collider.GetComponent<ProjectileI>())
			{
				Destroy(hit.collider.gameObject);
			}
			Destroy(this.gameObject);
        }
    }
}
