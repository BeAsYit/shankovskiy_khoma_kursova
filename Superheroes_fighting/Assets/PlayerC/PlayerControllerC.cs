﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControllerC : MonoBehaviour
{

    public Transform groundPivot;
    public Transform lowerGroundPivot;
    public LayerMask groundLayer;
    public float radiusCast = 1;

    // Projectiles
    public Transform projectilePivot;
    public ProjectileC projectilePrefab;
    public float attackSpeedTime = 0.5f;
    private float previousAttackTime = -0.6f;

    private PlayerC player;
    private Rigidbody2D playerRigidbody;
    private Animator playerAnim;

    private bool canDoubleJump = false;
    private bool grounded = false;

    // Use this for initialization
    void Start()
    {

        player = GetComponent<PlayerC>();
        playerRigidbody = GetComponent<Rigidbody2D>();
        playerAnim = GetComponent<Animator>();

    }

    // Update is called once per frame
    void Update()
    {

        if (player.dead)
        {
            return;
        }

        grounded = Physics2D.OverlapCircle(groundPivot.transform.position, radiusCast, groundLayer);

        float xMovement = Input.GetAxisRaw("HorizontalC");

        playerRigidbody.velocity = new Vector2(xMovement * player.movementSpeed, playerRigidbody.velocity.y);

        if (xMovement != 0)
        {
            transform.rotation = Quaternion.LookRotation(new Vector3(0, 0, xMovement));
        }

        playerAnim.SetFloat("speed", Mathf.Abs(xMovement));
        playerAnim.SetBool("Grounded", grounded);

        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            if (grounded)
            {
                playerRigidbody.velocity = new Vector2(playerRigidbody.velocity.x, player.jumpHeight);
				grounded = true;
                canDoubleJump = true;
            }
            else if (canDoubleJump)
            {
                playerRigidbody.velocity = new Vector2(playerRigidbody.velocity.x, player.jumpHeight);
                canDoubleJump = false;
            }

        }

		if (Input.GetKeyDown(KeyCode.End))
        {
            Fire();
        }
    }

    private void Fire()
    {
        if (Time.time - previousAttackTime > attackSpeedTime)
        {
            playerAnim.SetTrigger("attack");

            Instantiate(projectilePrefab, projectilePivot.transform.position, projectilePivot.transform.rotation);
            previousAttackTime = Time.time;
        }

    }

    void OnCollisionEnter2D(Collision2D col)
    {
        if (Physics2D.OverlapCircle(lowerGroundPivot.transform.position, radiusCast / 2, groundLayer) && canDoubleJump)
        {
			canDoubleJump = true;
        }
    }

#if UNITY_EDITOR
    void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(groundPivot.transform.position, radiusCast);
        Gizmos.DrawWireSphere(lowerGroundPivot.transform.position, radiusCast / 2);
    }
#endif
}
